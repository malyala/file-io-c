/***********************************************************
  Name: FileIO.h                                          
  Author: Amit Malyala                                     
  License: CC SA 3.0
  Description: 
  File IO  wrappers around low level fuctions 
  to read and write files which have similar functionality 
  of a C File IO functions.
  
  Notes:
  Classes and functions being designed.
  
  
  Write C++ fucntions which call fopen(), fclose(), fread(), fwrite() C functions and these C++ functions 
  should throw exceptions when errors occur.
  Version and bug history:
  0.1 Initial version.

***********************************************************/
#include <iostream>
#include <stdio.h>
#include <cstring>
#include <cstdlib>
#include <stdexcept>

// Define 
namespace FileIOstream
{
	// two versions, binary and unicode.
	// These defintions are to be defined	
	void fReadFile(const char* Filename);
	// two versions, binary and unicode.
	void fWriteFile(const char* Filename , char* data);
	std::size_t fwriteFile(const void* str, std::size_t size, std::size_t count, FILE* fptr);
	
	std::size_t freadFile(void* str, std::size_t size, std::size_t count, FILE* fptr);
	
	int fileclose(FILE* Fileptr);
	FILE* fileopen(const char* Filename, const char* type); 
   
}
