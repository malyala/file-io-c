/***********************************************************
  Name: app.cpp                                          
  Author: Amit Malyala                                     
  License: CC SA 3.0
  Description: 
  File IO  wrappers around low level fuctions 
  to read and write files which have similar functionality 
  of C File IO functions.
  
  Notes:
  
  Write C++ fucntions which call fopen(), fclose(), fread(), fwrite() C functions and these C++ functions 
  should throw exceptions when errors occur.
  Version and bug history:
  0.1 Initial version.

***********************************************************/
#include <iostream>
#include <stdio.h>
#include <cstring>
#include <cstdlib>
#include <stdexcept>
#include "app.h"



int main(void)
{
	std::cout << "Before app\n";
	TestReadWriteBin();
	std::cout << "\nAfter app\n";
	return 0;
}

/* Execute application */
void TestFileOpenclose(void) 
{
	try
	{
		int filecloseerrorstatus= 0;
		FILE *fp = FileIOstream::fileopen("name.txt","r");
		std::cout << "Executing app\n";
		if (fp)
		{
			char c; 
            while ((c = fgetc(fp)) != EOF) 
			{
			    // standard C I/O file reading loop
			    std::cout << c;
            }
		}
		else
		{
			throw std::runtime_error("File open error");
		}
		
		if (ferror(fp))
		{
			throw std::runtime_error("I/O error when reading");
		}
		else if (feof(fp))
		{
			std::cout << "End of file reached successfully\n";
		}
		
		filecloseerrorstatus = FileIOstream::fileclose(fp);
		if (filecloseerrorstatus == EOF)
		{
			throw std::runtime_error("File close error");
		}
	}
	catch (std::exception const& e)
    {
        std::cout << e.what() ;
    }
}

/* Test Read files */
void TestReadWriteBin(void)
{
	try
	{
		std::size_t SIZE =5;
		int filecloseerrorstatus= 0;
		std::size_t fileWriteStatus=0;
		char a[SIZE] =  {'a','b','c','d','e'};
		FILE *fp = FileIOstream::fileopen("name.bin","wb");
		fileWriteStatus= FileIOstream::fwriteFile(a,sizeof *a, SIZE,fp);
		if (fileWriteStatus != SIZE)
		{
			throw std::runtime_error("Error writing test.bin");
		}
		filecloseerrorstatus = FileIOstream::fileclose(fp);
		if (filecloseerrorstatus == EOF)
		{
			throw std::runtime_error("File close error");
		}
		char b[SIZE];
		fp = FileIOstream::fileopen("name.bin","rb");
		size_t ret_code = fread(b, sizeof *b, SIZE, fp); // reads an array of chars
		if(ret_code == SIZE) 
		{
            std::cout << ("Array read successfully, contents: ");
            for(std::size_t n = 0; n < SIZE; ++n) 
            {
            	std::cout << b[n] << '\n';            	
			}
        } 
		else 
		{    // error handling
           if (feof(fp))
           {
           	  throw std::runtime_error("Error reading test.bin: unexpected end of file.\n");           	
		   }
           else if (ferror(fp)) 
		   {
		      throw std::runtime_error("Error reading test.bin");
           }
        }
		filecloseerrorstatus = FileIOstream::fileclose(fp);
		if (filecloseerrorstatus == EOF)
		{
			throw std::runtime_error("File close error");
		}
	}
	catch (std::exception const& e)
    {
        std::cout << e.what() ;
    }
}


