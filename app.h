/***********************************************************
  Name: app.h                                           
  Author: Amit Malyala                                     
  Description: 
  File IO libray wrappers around low level fuctions 
  to read and write files. 
  
Version and bug history:
0.1 Initial version.
***********************************************************/
#include "Fileio.h"

/* Execute application */
void TestFileOpenclose(void);


/* Read and write to files */
void TestReadWriteBin(void);
