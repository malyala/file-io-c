/***********************************************************
  Name: FileIO.cpp                                        
  Author: Amit Malyala                                     
  License: CC SA 3.0
  
  Description: 
  File IO libray wrappers around low level fuctions 
  to read and write files. 
  These functions throw exceptions for error condtions.
  Error conditions are below:
  File not found
  File opening error  - unable to open
  File already open - error 
  Disk full (file system error)
  No write permissions on disk
  Writing on read only file 
  Data corruption in file stream.
  Error during data transfer with number of bytes transferred.
  Create file error because a file with same name exists
  File closing error - unable to close
  File already closed error.
  Error for incorrect arguments 
  Error for incorrect file permissions
  wrong type of argument in fopen 
  wrong type of argument in fclose 
  Checksum error while reading or writing data.?
  Add no throw /throw features in file IO functions. 
  
  
  Notes:
  When throwing a exception after opening a file, make sure that the file is closed before a exception is thrown
  to avoid memory leaks.
  
  
  Coding log:
  03-11-20 created initial version.
  
  
  
Version and bug history:
0.1 Initial version.

***********************************************************/
#include "FileIO.h"




/* Function to open a file */
FILE* FileIOstream::fileopen(const char* name, const char* type)
{
	FILE* fp = fopen(name, type);
	return fp;
}

std::size_t FileIOstream::fwriteFile(const void* str, std::size_t size, std::size_t count, FILE* fptr)
{
	std::size_t ret_code = fwrite(str, size, count ,fptr); // reads an array of chars
	return ret_code;
}

std::size_t FileIOstream::freadFile(void* str, std::size_t size, std::size_t count, FILE* fptr)
{
		std::size_t ret_code = fread(str,size,count,fptr);
		return ret_code;
}
/* Function to close a file */
int FileIOstream::fileclose(FILE* Fileptr)
{
	int filecloseerrorstatus = 0;
	filecloseerrorstatus =fclose(Fileptr);
	if (filecloseerrorstatus)
	{
		throw std::runtime_error("File close error exception");
	}
	return filecloseerrorstatus;
}

